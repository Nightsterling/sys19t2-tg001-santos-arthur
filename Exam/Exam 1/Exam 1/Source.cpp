#include <iostream>
#include <string>
#include <ctime>


using namespace std;

int main()
{
	srand(time(0));
	int playerHp;
	cout << "Input player HP: ";
	cin >> playerHp;

	int minplayerDmg;
	cout << "Input min player damage:";
	cin >> minplayerDmg;

	int maxplayerDmg;
	cout << "Input max player damage:";
	cin >> maxplayerDmg ;

	int enemyHp;
	cout << "Input enemy HP: ";
	cin >> enemyHp;

	int minenemyDmg;
	cout << "Input min enemy damage:";
	cin >> minenemyDmg;

	int maxenemyDmg;
	cout << "Input max enemy damage:";
	cin >> maxenemyDmg;
	
	while (playerHp > 0 && enemyHp > 0)
	{
		
		int a;
		int pdamage = rand() % (maxplayerDmg - minplayerDmg) + minplayerDmg + 1;
		int edamage = rand() % (maxenemyDmg - minenemyDmg) + minenemyDmg + 1;
		int enemyaction = rand() % 3 + 1;
		bool A = false;
		bool D = false;
		bool W = false;
		char playerinput;
		cout << "Player HP = " << playerHp << endl;
		cout << "Enemy HP = " << enemyHp << endl;
		cout << "Choose your action" << endl;
		cout << "===================" << endl;
		cout << "        [a] = Attack" << endl;
		cout << "        [d] = Defend" << endl;
		cout << "        [w] = Wild Attack" << endl;
		cin >> playerinput;
		
		if (playerinput == 'a' || playerinput == 'A')
		{
			A = true;
		}
		else if (playerinput == 'd' || playerinput == 'D')
		{
			D = true;
		}
		else if (playerinput == 'w' || playerinput == 'W')
		{
			W = true;
		}
		else
		{
			cout << "Invalid input" << endl;
		}

		if (A && enemyaction == 1)
		{
			cout << "Player used Attack and dealt:" << pdamage << " damage" << endl;
			cout << "Enemy used Attack and dealt:" << edamage << " damage" << endl;
			playerHp = playerHp - edamage;
			enemyHp = enemyHp - pdamage;
			if (playerHp < 0)
			{
				playerHp = 0;
			}

			if (enemyHp < 0)
			{
				enemyHp = 0;
			}
			cout << "Player HP = " << playerHp << endl;
			cout << "Enemy HP = " << enemyHp << endl;
			cout << "===================" << endl;
		}

		else if (A && enemyaction == 2)
		{
			cout << "Player used Attack and dealt:" << pdamage / 2 << " damage" << endl;
			cout << "Enemy defended:" << endl;
			enemyHp = enemyHp - (pdamage / 2);
			if (playerHp < 0)
			{
				playerHp = 0;
			}

			if (enemyHp < 0)
			{
				enemyHp = 0;
			}
			cout << "Player HP = " << playerHp << endl;
			cout << "Enemy HP = " << enemyHp << endl;
			cout << "===================" << endl;
		}
		else if (A && enemyaction == 3)
		{
			cout << "Player used Attack and dealt:" << pdamage << " damage" << endl;
			cout << "Enemy used Wild Attack and dealt:" << edamage * 2 << " damage" << endl;
			playerHp = playerHp - (edamage * 2);
			enemyHp = enemyHp - pdamage;
			if (playerHp < 0)
			{
				playerHp = 0;
			}

			if (enemyHp < 0)
			{
				enemyHp = 0;
			}
			cout << "Player HP = " << playerHp << endl;
			cout << "Enemy HP = " << enemyHp << endl;
			cout << "===================" << endl;
		}
		else if (D && enemyaction == 1)
		{
			cout << "Player defended:" << endl;
			cout << "Enemy used Attack and dealt:" << edamage/2 << " damage" << endl;
			playerHp = playerHp - edamage/2;
			if (playerHp < 0)
			{
				playerHp = 0;
			}

			if (enemyHp < 0)
			{
				enemyHp = 0;
			}
			cout << "Player HP = " << playerHp << endl;
			cout << "Enemy HP = " << enemyHp << endl;
			cout << "===================" << endl;
		}

		else if (D && enemyaction == 2)
		{
			cout << "Player defended:" << endl;
			cout << "Enemy defended:" << endl;
			cout << "Nothing happened" << endl;
			cout << "Player HP = " << playerHp << endl;
			cout << "Enemy HP = " << enemyHp << endl;
			cout << "===================" << endl;
		}
		else if (D && enemyaction == 3)
		{
			cout << "Player Countered and dealt:" << pdamage * 2 << " damage" << endl;
			cout << "Enemy used Wild Attack and was countered:" << endl;
			enemyHp = enemyHp - (pdamage * 2);
			if (playerHp < 0)
			{
				playerHp = 0;
			}

			if (enemyHp < 0)
			{
				enemyHp = 0;
			}
			cout << "Player HP = " << playerHp << endl;
			cout << "Enemy HP = " << enemyHp << endl;
			cout << "===================" << endl;
		}

		else if (W && enemyaction == 1)
		{
			cout << "Player used Wild Attack and dealt:" << pdamage * 2 << " damage" << endl;
			cout << "Enemy used Attack and dealt:" << edamage << " damage" << endl;
			playerHp = playerHp - edamage;
			enemyHp = enemyHp - pdamage * 2;
			if (playerHp < 0)
			{
				playerHp = 0;
			}

			if (enemyHp < 0)
			{
				enemyHp = 0;
			}
			cout << "Player HP = " << playerHp << endl;
			cout << "Enemy HP = " << enemyHp << endl;
			cout << "===================" << endl;
		}

		else if (W && enemyaction == 2)
		{
			cout << "Player used Wild Attack and was countered:" << endl;
				cout << "Enemy countered and dealt:" << edamage * 2 << " damage" << endl;
				playerHp = playerHp - (edamage * 2);
				if (playerHp < 0)
				{
					playerHp = 0;
				}

				if (enemyHp < 0)
				{
					enemyHp = 0;
				}
				cout << "Player HP = " << playerHp << endl;
				cout << "Enemy HP = " << enemyHp << endl;
				cout << "===================" << endl;
		}
		else if (W && enemyaction == 3)
		{
			cout << "Player used Wild Attack and dealt:" << pdamage * 2 << " damage" << endl;
			cout << "Enemy used Wild Attack and dealt:" << edamage * 2 << " damage" << endl;
			playerHp = playerHp - (edamage * 2);
			enemyHp = enemyHp - (pdamage * 2);
			if (playerHp < 0)
			{
				playerHp = 0;
			}

			if (enemyHp < 0)
			{
				enemyHp = 0;
			}
			cout << "Player HP = " << playerHp << endl;
			cout << "Enemy HP = " << enemyHp << endl;
			cout << "===================" << endl;
		}
		

		system("pause");
		system("cls");
		

	}
	if (playerHp <= 0 && enemyHp <= 0)
	{
		cout << "It's a draw!" << endl;
	}
	
	else if (enemyHp <= 0)
	{
		cout << "Congratulations you beat the AI!" << endl;
	}
	else if (playerHp <= 0)
	{
		cout << "The AI won better luck next time." << endl;
	}

		system("pause");
		return 0;
		
}
