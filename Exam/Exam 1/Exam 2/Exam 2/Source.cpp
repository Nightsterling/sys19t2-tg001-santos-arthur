#include <iostream>
#include <string>
#include <ctime>

using namespace std;
struct Roll
{
	string type; // Snake Eyes, Triples, Pairs ,etc
	int rank; // Higher is better. Snake Eyes has higher ranking than Triples
	int value; // The value of the roll for Triples and Pairs (eg. 6-4-4 value is 6)
	int dice[3]; // The actual value rolled for each die
};

void cinClear() {
	cin.clear();
	cin.ignore('\n', 10);
}

int Wager(int money)
{
	int bet;
	do
	{
		cout << "Please Enter your bet :  (Minumum is 100 perica!!)" << endl;
		cin >> bet;
	} while (bet < 100 || bet > money);

	return bet;
}


Roll RollDice();

int pairCheck(int roll[]);

int consecutiveCheck(int roll[]);

int triplesCheck(int roll[]);

void PrintScore(int rollscore)
{
	if (rollscore >= 1 && rollscore <= 6)
	{
		cout << "Score : " << rollscore;
	}
	else
	{
		switch (rollscore)
		{
		case (-2):
			cout << "Its a consecutive but you lose x2 !";
			break;
		case (-1):
			cout << "Its a piss :(";
			break;
		case (7):
			cout << "Its a consecutive !";
			break;
		case (8):
			cout << "Its a Triple ! ";
			break;
		case (9):
			cout << "Snake Eyes ! ";
			break;
		default:cout << "Bust";
		}
	}
	cout << endl;
}
void PrintRoll(Roll roll)
{
	for (int i = 0; i < 3; i++)
	{
		cout << roll.dice[i] << ",";
	}
	PrintScore(roll.value);

}

int SingleDice()
{
	return rand() % 6 + 1;

}

bool isPiss() {
	return rand() % 100 < 5;
}

int getScore(int roll[]) {
	int score = pairCheck(roll);

	if (isPiss()) {
		score = -1;
	}
	else {
		if (score == 0)
			score = consecutiveCheck(roll);
		if (score == 0)
			score = triplesCheck(roll);
	}
	return score;
}

void Ending(int money)
{
	if (money > 500000)
	{
		cout << "Best Ending" << endl;
	}
	if (money >= 90000 && money < 500000)
	{
		cout << "Good Ending" << endl;
	}
	if (money < 90000)
	{
		cout << "OK Ending" << endl;
	}
	if (money == 0)
	{
		cout << "Bad Ending" << endl;
	}
	if (money < 0)
	{
		cout << "Worst Ending " << endl;
	}

}

int WinPayout(int playerscore, int dealerscore)
{
	if (playerscore == -1 && dealerscore == -2)
	{
		return 0;
	}
	else
		if (playerscore >= 1 && playerscore <= 6)
		{
			return 1;
		}
		else
		{
			switch (playerscore)
			{
			case (7):
				return 2;
			case (8):
				return 3;
			case(9):
				return 5;
			default: return 1;

			}
		}
}

int DrawPayout(int rollscore)
{
	if (rollscore == -2 || rollscore == 0)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int LosePayout(int playerscore, int dealerscore)
{
	if (playerscore == -1 && dealerscore == 0)
	{
		return 0;
	}
	else
	{
		return -1;
	}
}

int CompareRoll(Roll playerRoll, Roll dealerRoll) {
	if (playerRoll.value > dealerRoll.value)
	{
		cout << "YOU WIN" << endl;
		return WinPayout(playerRoll.value, dealerRoll.value);
	}
	else if (playerRoll.value < dealerRoll.value)
	{
		cout << "YOU LOSE" << endl;
		return LosePayout(playerRoll.value, dealerRoll.value);
	}
	else
	{
		cout << "DRAW" << endl;
		return DrawPayout(playerRoll.value);
	}

}
Roll RollUntilValid()
{
	Roll roll;
	roll.value = 0;
	for (int i = 0; i < 3 && roll.value == 0; i++)
	{
		roll = RollDice(); PrintRoll(roll);
		if (roll.value == -1)
		{
			i = 3;
		}
	}
	return roll;
}

int main()
{
	srand(time(0));
	Roll playerRoll, dealerRoll;
	int money = 90000;
	int wager = 0;
	int rounds = 10;
	int payout;

	for (int i = 1; i <= rounds; i++)
	{
		cout << "Current Money: " << money << endl;
		wager = Wager(money);
		playerRoll.value = 0;
		cout << "Ohtsuki Roll :" << endl;
		dealerRoll = RollUntilValid();
		system("pause>nul");
		system("cls");
		if (dealerRoll.value != -1)
		{
			cout << "Kaiji Roll :" << endl;
			playerRoll = RollUntilValid();
			system("pause>nul");
			system("cls");
			cout << " Ohtsuki Roll:" << endl; PrintRoll(dealerRoll);
			cout << " Kaiji Roll:" << endl; PrintRoll(playerRoll);
		}
		payout = CompareRoll(playerRoll, dealerRoll);
		money += wager * payout;

	}
	cout << money << endl;
	Ending(money);
	system("pause");
	return 0;
}

Roll RollDice() {
	Roll roll;
	//step 1: insert dice values in roll array;
	for (int i = 0; i < 3; i++)
	{
		roll.dice[i] = SingleDice();
	}
	//step 2: determine score
	roll.value = getScore(roll.dice);
	return roll;
}

int pairCheck(int roll[])
{
	int score = 0;
	if (roll[0] == roll[1]) score = roll[2];
	else if (roll[1] == roll[2]) score = roll[0];
	else if (roll[0] == roll[2]) score = roll[1];
	else score = 0;
	return score;
}

int consecutiveCheck(int roll[])
{
	int score = 0;
	for (int i = 3 - 1; i >= 0; i--)
	{
		for (int j = 0; j < i; j++)
		{
			if (roll[j] > roll[j + 1])
			{
				int temp = roll[j];
				roll[j] = roll[j + 1];
				roll[j + 1] = temp;
			}
		}

	}
	if (roll[0] == 4)
	{
		score = 7;
		for (int i = 1; i < 3; i++)
		{
			if (roll[i] - 1 != roll[i - 1])
			{
				score = 0;
				break;
			}

		}
	}
	else if (roll[0] == 1)
	{
		score = -2; // 1,2,3
		for (int i = 1; i < 3; i++)
		{
			if (roll[i] - 1 != roll[i - 1])
			{
				score = 0;
				break;
			}

		}
	}


	return score;
}

int triplesCheck(int roll[])
{
	int score = roll[0];
	for (int i = 0; i < 3; i++)
	{
		if (score != roll[i])
		{
			score = 0;
			break;
		}
	}
	if (score != 0)
	{
		if (score == 1)
		{
			score = 9; // 1,1,1
		}
		else
		{
			score = 8; // 2,2,2
		}
	}

	return score;
}
