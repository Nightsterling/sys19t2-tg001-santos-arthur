#include <iostream>
#include <string>

using namespace std;

int main()
{
	
	string firstName;
	string lastName;
	int age;
	float money;
	bool breakfast;

	
	cout << "What is your first name? ";
	cin >> firstName;
	cout << "What is your last name? ";
	cin >> lastName;
	cout << "How old are you? ";
	cin >> age;
	cout << "How much do you have in you (money)? ";
	cin >> money;
	cout << "Have you eaten breakfast today? ";
	cin >> breakfast;

	
	system("cls");

	
	cout << "Name:" << lastName << ", " << firstName << endl;
	cout << "Age:" << age << endl;
	cout << "Money:" << money;
	cout << "Has eaten breakfast:" << breakfast << endl;

	system("pause");
	return 0;