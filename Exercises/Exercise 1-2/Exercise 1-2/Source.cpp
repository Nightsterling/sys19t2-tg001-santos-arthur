#include <iostream>
#include <string>

using namespace std;

int main()
{
	
	int x;
	int y;

	cout << "Input x: ";
	cin >> x;
	cout << "Input y: ";
	cin >> y;
	
	int sum = x + y;
	int diff = x - y;
	int product = x * y;
	int quotient = x / y;
	int mod = x % y;

	cout << x << " + " << y << " = " << sum << endl;
	cout << x << " - " << y << " = " << diff << endl;
	cout << x << " * " << y << " = " << product << endl;
	cout << x << " / " << y << " = " << quotient << endl;
	cout << x << " % " << y << " = " << mod << endl;

	system("pause");
	return 0;
}