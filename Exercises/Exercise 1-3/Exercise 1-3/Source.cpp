#include <iostream>
#include <string>

using namespace std;

int main()
{
	const float exchangeRate = 47.74f;

	float usd;
	cout << "Input USD: ";
	cin >> usd;

	float php = usd * exchangeRate;

	cout << "PHP: " << php << endl;

	system("pause");
	return 0;
}