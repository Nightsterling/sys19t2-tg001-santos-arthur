#include <iostream>
#include <string>

using namespace std;

int main()
{
	int x = 0;
	int y = 0;

	bool exit = false;
	while (!exit)
	{
		char input;
		cout << "Position (" << x << ", " << y << ")" << endl;
		cout << "Input (w,a,s,d,x): ";
		cin >> input;

		switch (input)
		{
		case 'w':
		case 'W':
			y++;
			break;
		case 's':
		case 'S':
			y--;
			break;
		case 'a':
		case 'A':
			x--;
			break;
		case 'd':
		case 'D':
			x++;
			break;
		case 'x':
		case 'X':
			exit = true;
			break;
		default:
			cout << "Invalid input!" << endl;
			break;
		}
	}

	system("pause");
	return 0;
}