#include <iostream>

using namespace std;

int main()
{
    int a;
    int num;
    cout << "Print all odd numbers from 1 to ";
    cin >> num;

    for (a = 1; a <= num; a++)
    {
        if (a % 2 == 1)
        {
            cout << a << endl;
        }
    }

    return 0;
}
