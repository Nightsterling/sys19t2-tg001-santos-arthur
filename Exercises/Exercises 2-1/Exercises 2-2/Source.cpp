#include <iostream>
#include <string>

using namespace std;

int main()
{
	int x;
	int y;
	cout << "Input x: ";
	cin >> x;
	cout << "Input y: ";
	cin >> y;

	cout << x << " == " << y << " = " << (x == y) << endl;
	cout << x << " != " << y << " = " << (x != y) << endl;
	cout << x << " > " << y << " = " << (x > y) << endl;
	cout << x << " < " << y << " = " << (x < y) << endl;
	cout << x << " >= " << y << " = " << (x >= y) << endl;
	cout << x << " <= " << y << " = " << (x <= y) << endl;

	int hp = 50;
	bool alive = !(hp <= 0);

	system("pause");
	return 0;
}