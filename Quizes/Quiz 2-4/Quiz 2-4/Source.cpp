#include <iostream>
#include <string>

using namespace std;

int main()
{
	string std = "Hello World";
	
	cout << std << endl;

	float x = 5.0f;
	cout << x << endl;

	int num65 = 65;

	cout << "num65" << " = " << num65 << endl;

	system("pause");
	return 0;
}
