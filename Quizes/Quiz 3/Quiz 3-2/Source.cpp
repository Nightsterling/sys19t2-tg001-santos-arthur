#include <iostream>

using namespace std;

int main()
{
	int maxCollumn = 8;

	for (int i = 0; i < maxCollumn-2; i++)
	{
		for (int a = 0; a < maxCollumn; a++)
		{
			if ((i + a) % 2 == 0)
			{
				cout << "X";
			}
			else
			{
				cout << "O";
			}
		}
		cout << "\n";
	}
	system("pause");
	return 0;
}