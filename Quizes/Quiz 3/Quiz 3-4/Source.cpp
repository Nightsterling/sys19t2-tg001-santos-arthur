#include <iostream>

using namespace std;

int main()
{
	int term1 = 0;
	int term2 = 1;
	int sum = 0;
	int terms;
	cout << "Enter number of terms: ";
	cin >> terms;

	for (int i = 0; i <= terms; i++)
	{
		if (i == 0)
		{
			cout << term1 << " ";
			continue;
		}
		if (i == 1)
		{
			cout << term2 << " ";
			continue;
		}
		int sum = term1 + term2;
		term1 = term2;
		term2 = sum;
		cout << sum << " ";
	}
		system("pause");
	return 0;
}